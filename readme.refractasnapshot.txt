v. 10.2.0+
Refracta Snapshot creates a bootable live-CD image which is a copy of
your running system. Any changes you make to the running system, 
including desktop preferences, configuration changes, or added software
will be copied to the snapshot and will be present when you run that
snapshot as a live system.

See the config file, /etc/refractasnapshot.conf for setting options.
The config file is the most up-to-date and detailed documentation.

See the excludes file, /usr/lib/refractasnapshot/snapshot_exclude.list
to examine which files and directories will not be copied to the 
snapshot. Edit the file as needed, either by adding items or commenting
out listed items that you want copied.

As of version 9.0.8, you can edit the config and excludes files from
within the gui program by choosing "Setup".

To run the script:

	refractasnapshot
or
	refractasnapshot-gui
	
or use the full path if needed:
	/usr/bin/refractasnapshot(-gui)
	
	
COMMAND LINE OPTIONS

	-h, --help		show help
	-v, --version	display the version information
	-c, --config	specify a different config file
					(file name must be next argument)
	-d, --debug		run in debug mode (set -x)


TROUBLESHOOTING

Run refractasnapshot or refractasnapshot-gui with the '-d' option and
save /var/log/refractasnapshot.log for analysis.


NOCOPY OPTION

The nocopy option has been replaced with an options menu in the script.
 1. Create a snapshot 
	- This runs the full process to create a snapshot, consisting of
		rsync copy to create the live filesystem
		copy isolinux boot files
		create efi boot files
		squash the live filesystem
		put it all into a bootable iso file.
 2. Re-squash and make iso (no-copy) 
	- For manual changes to the live filesystem in $work_dir/myfs.
 3. Re-make efi files and iso (no-copy, no-squash)
	- Maybe for hacking on the mkefi function in the script.
 4. Re-run xorriso only (make iso, no-copy, no-squash) 
	- For manual changes to any files in the root of the live media,
	such as boot menus or boot help files in $work_dir/iso.

The nocopy options will prevent rsync from updating the saved copy of the
filesystem. Any system changes or updates that occurred after the copy 
was made will not be included in the final image.

The nocopy options are for special purposes (for example, you made changes
to the saved copy of the filesystem after a previous snapshot, and you 
don't want those changes overwritten by the running system.) 
However, you must have save_work=yes and have a previously saved copy of
the filesystem for nocopy to work.


CREATING A SNAPSHOT FROM AN ENCRYPTED SYSTEM OR SWAP PARTITION

If your host system is encrypted or has a swap partition, the script
will edit the snapshot's initrd to remove conf/conf.d/cryptroot and/or
conf/conf.d/resume so that the snapshot iso will boot successfully.

This will NOT affect the initrd on the host system.


CREATING A SNAPSHOT TO BE USED WITH ENCRYPTED PERSISTENCE

If you want to create an iso that will be used to make a live-usb with
an encrypted volume for persistence, you need to rebuild the initrd to
include cryptsetup if it's missing. This is now automatic. 

- Uncomment the line in /etc/refractasnapshot.conf that contains:
		initrd_crypt="yes"
		
The script will check for cryptsetup in the initrd, and if necessary
will edit /etc/cryptsetup-initramfs/conf-hook to set CRYPTSETUP=y.

This DOES change the host's initrd. To revert the change in the host,
comment out the line and run 'update-initramfs -u'.

Assumptions: The kernel you're running is the one that you will use in
the snapshot. (i.e. /initrd.img)


NETWORK CONFIGURATIONS

If you want custom network configurations to be copied into the snapshot,
uncomment the line in the config file that has:
	netconfig_opt="ip=frommedia"

This will preserve your /etc/network/interfaces and any saved wireless
configurations. This works for NetworkManager, simple-netaid/netman
and wicd. It will also add "ip=frommedia" to the boot command, so that 
the saved configuration will be used in the live system.

If you leave the line commented, the default behavior is to
replace the interfaces file with one that only has the loopback interface
configured and to delete any saved wireless configurations. 
(This only affects the filesystem copy in $work_dir/myfs.)

NOTE!!! If you're using some other network manager, and you don't want
your configs to be copied, you need to add the appropriate files to
the excludes list. (Tell me what those files are, and I'll fix 
refractasnapshot to handle it.)


NETWORK INTERFACE NAMES

To control whether the snapshot uses old network interface names like
eth0 or new names like enp0s1, uncomment and set ifnames_opt in the 
config file.

Use net.ifnames=0 to force old interface names with udev. (eth0)
Use net.ifnames=1 to force new interface names with eudev. (enp0s1)


